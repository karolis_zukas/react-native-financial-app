import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

interface Props {
    name: string;
}

const Screen = (props: Props) => {
    const { name } = props;
    return (
        <View style={styles.container}>
            <Text style={styles.text}>This is a {name} screen</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ea3345',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 28,
        color: '#222222',
        textAlign: 'center'
    }
});

export default Screen;
