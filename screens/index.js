import React from 'react';

import Screen from './Screen';

export const HomeScreen = () => <Screen name="Overview" />;
export const SearchScreen = () => <Screen name="Stats" />;
export const PaymentsScreen = () => <Screen name="Payments" />;
export const FavoritesScreen = () => <Screen name="Assets" />;
export const ProfileScreen = () => <Screen name="Profile" />;