import { Dimensions, Platform } from 'react-native';

interface Idim {
    width: number;
    height: number;
} 

export const isIphoneX = (): boolean => {
    const dim = Dimensions.get('window');
    return (Platform.OS === 'ios' && (isIPhoneXSize(dim) || isIPhoneXrSize(dim)));
  }

  export const isIPhoneXSize = (dim: Idim): boolean => {
    return dim.height == 812 || dim.width == 812;
  }

  export const isIPhoneXrSize = (dim: Idim): boolean => {
    return dim.height == 896 || dim.width == 896;
  }