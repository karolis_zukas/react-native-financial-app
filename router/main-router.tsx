import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Feather as Icon } from '@expo/vector-icons';

import Tabbar from '../components/Tabbar';

import {
    HomeScreen,
    SearchScreen,
    PaymentsScreen,
    FavoritesScreen,
    ProfileScreen
} from '../screens';

const MainRouter = createBottomTabNavigator(
    {
        HomeScreen: {
            screen: HomeScreen,
            navigationOptions: {
                title: 'Overview',
                tabBarIcon: ({ tintColor }) => <Icon size={25} name="grid" color={tintColor} />
            }
        },
        SearchScreen: {
            screen: SearchScreen,
            navigationOptions: {
                title: 'Stats',
                tabBarIcon: ({ tintColor }) => <Icon size={25} name="list" color={tintColor} />
            }
        },
        PaymentsScreen: {
            screen: PaymentsScreen,
            navigationOptions: {
                title: 'Payments',
                tabBarIcon: ({ tintColor }) => <Icon size={25} name="refresh-ccw" color={tintColor} />
            }
        },
        FavoritesScreen: {
            screen: FavoritesScreen,
            navigationOptions: {
                title: 'Assets',
                tabBarIcon: ({ tintColor }) => <Icon size={25} name="box" color={tintColor} />
            }
        },
        ProfileScreen: {
            screen: ProfileScreen,
            navigationOptions: {
                title: 'Profile',
                tabBarIcon: ({ tintColor }) => <Icon size={25} name="user" color={tintColor} />
            }
        }
    },
    {
        initialRouteName: 'HomeScreen',
        tabBarComponent: Tabbar,
        tabBarOptions: {
            activeTintColor: 'black',
            inactiveTintColor: '#c1c1c1'
        }
    });

export default createAppContainer(MainRouter);
