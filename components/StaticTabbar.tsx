import React from 'react';
import {
    View,
    TouchableWithoutFeedback,
    StyleSheet,
    Animated,
    Dimensions,
    Text
} from 'react-native';
import { isIphoneX } from '../utils/Sizes';

interface Props {
    navigation: any;
    value: Animated.Value;
    renderIcon: Function;
    activeTintColor?: string;
    inactiveTintColor?:  string;
    onTabPress?: any;
    getLabelText: Function;
}

export const tabHeight = isIphoneX() ?  76 : 64;
const { width } = Dimensions.get('window');

const StaticTabbar = (props: Props) => {
    const { navigation, value, renderIcon, inactiveTintColor, activeTintColor, onTabPress, getLabelText } = props;
    const { routes } = navigation.state;
    const activeRouteIndex = navigation.state.index;
    const tabWidth = width / routes.length;

    let values: Animated.Value[] = routes.map((route, index) => new Animated.Value(index === activeRouteIndex ? 1 : 0));

    const onPress = (index: number) => {
        Animated.sequence([
            Animated.parallel(
            values.map(v => Animated.timing(v, {
                toValue: 0,
                duration: 250
            })),
            ),
            Animated.parallel([
                Animated.spring(value, {
                    toValue: -width + tabWidth * index,
                    restSpeedThreshold: 100,
                    restDisplacementThreshold: 40
                }),
                Animated.spring(values[index], {
                    toValue: 1,
                    restSpeedThreshold: 100,
                    restDisplacementThreshold: 40
                }),
            ]),
        ]).start(() => {
            onTabPress({ route: routes[index] });
        });
    }

    return (
        <View style={styles.container}>{
            routes.map((route, key) => {
                const opacity = value.interpolate({
                    inputRange: [
                        -width + tabWidth * (key - 1),
                        -width + tabWidth * key,
                        -width + tabWidth * (key + 1)
                    ],
                    outputRange: [1, 0, 1],
                    extrapolate: 'clamp'
                });
                const translateY = values[key].interpolate({
                    inputRange: [0, 1],
                    outputRange: [tabHeight, 0],
                    extrapolate: 'clamp'
                });
                const opacity1 = values[key].interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 1],
                    extrapolate: 'clamp',
                });

                return (
                    <React.Fragment {...{ key }}>
                        <TouchableWithoutFeedback onPress={() => onPress(key)}>
                            <Animated.View style={[styles.tab, styles.flex, { opacity }]}>
                                {renderIcon({ route, tintColor: inactiveTintColor })}
                                <Text style={ styles.inactiveLabelText }>{getLabelText({ route })}</Text>
                            </Animated.View>
                        </TouchableWithoutFeedback>
                        <Animated.View style={[
                            styles.tab,
                            styles.highlighted,
                            { width: tabWidth, left: tabWidth * key },
                            { opacity: opacity1 },
                            {transform: [{ translateY }]}
                        ]}>
                            <Text style={ styles.activeLabelText }>{getLabelText({ route })}</Text>
                            <View style={ styles.circle }>{ renderIcon({ route, tintColor: activeTintColor }) }</View>
                        </Animated.View>
                    </React.Fragment>
                    )
                })
        }</View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    tab: {
        height: tabHeight,
        paddingBottom: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    flex: {
        flex: 1
    },
    highlighted: {
        position: 'absolute',
        top: -15
    },
    circle: {
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    activeLabelText: {
        color: 'white',
        marginBottom: 3,
        fontWeight: '500'
    },
    inactiveLabelText: {
        color: '#c1c1c1'
    }
});

export default StaticTabbar;
