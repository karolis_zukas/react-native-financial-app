import React from 'react';
import { Dimensions, View, Animated, StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import * as shape from 'd3-shape';

import { isIphoneX } from '../utils/Sizes';
import StaticTabbar, { tabHeight as height } from './StaticTabbar';

const { width } = Dimensions.get('window');
const AnimatedSvg = Animated.createAnimatedComponent(Svg);

interface Props {
    renderIcon?: any;
    activeTintColor?: string;
    inactiveTintColor?: string;
    onTabPress?: any;
    onTabLongPress?: any;
    navigation: any;
    getLabelText: Function;
}

const Tabbar = (props: Props) => {
    const { onTabPress, navigation, renderIcon, activeTintColor, inactiveTintColor, getLabelText } = props;
    const { routes, index } = navigation.state;
    const tabWidth = width / routes.length;
    let value = new Animated.Value(-width + (tabWidth * index));

    const left = shape.line().x(d => d.x).y(d => d.y)([
        { x: 0, y: 0},
        { x: width, y: 0}
    ]);

    const right = shape.line().x(d => d.x).y(d => d.y)([
        { x: width + tabWidth, y: 0},
        { x: width * 2.5 , y: 0},
        { x: width * 2.5 , y: height },
        { x: 0 , y: height },
        { x: 0 , y: 0 }
    ]);

    const tab = shape.line().x(d => d.x).y(d => d.y).curve(shape.curveBasis)([
        { x: width - 10, y: 0 },
        { x: width - 5, y: 0 },
        { x: width + 10, y: 10 },
        { x: width + 15, y: (isIphoneX() ? height - 33 : height - 21) },
        { x: width + tabWidth - 15, y: (isIphoneX() ? height - 33 : height - 21) },
        { x: width + tabWidth - 10, y: 10 },
        { x: width + tabWidth + 5, y: 0 },
        { x: width + tabWidth + 10, y: 0 },
    ]);

    const d = `${left} ${tab} ${right}`;

    return (
        <View {...{ width, height }} style={styles.container}>
            <AnimatedSvg
                style={{transform: [{translateX: value}]}}
                width={width * 2.5}
                {...{ height }}>
                <Path {...{ d }} fill="white" />
            </AnimatedSvg>
            <View style={StyleSheet.absoluteFill}>
                <StaticTabbar {...{ value, onTabPress, navigation, renderIcon, activeTintColor, inactiveTintColor, getLabelText }} />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0)',
        position: 'absolute',
        left: 0,
        bottom: 0
    }
})

export default Tabbar;
